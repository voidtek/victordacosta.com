# victordacosta.com

This is the source code for [victordacosta.com](http://victordacosta.com). The site is fully static, powered by [Hugo](https://gohugo.io) and hosted on [Gitlab](https://gitlab.com/).

## Installation
Clone the repository and enter its directory

...
